# foobartory

## Lancer l'application

### Pré-requis
- wget : téléchargement de la dernière version
- Java 11 : exécution de l'application

### Commande

L'affichage se fait sur la sortie standard

```shell
./run.sh
```

Le jeu se déroule en temps réel.

On peut accélérer le déroulement en passant dans la variable d'environnement FOOBARTORY_SPEED un facteur d'accélération.

Exemple :

```shell
FOOBARTORY_SPEED=10 ./run.sh
```

Le jeu s'écoulera 10 fois plus rapidement

## Outillage mis en oeuvre

- Application Kotlin/Maven
- Utilisation de gitlab CI/CD pour construire un jar à chaque push sur le repository. Le jar créé est stocké dans les artifacts gitlab et est récupéré puis utilisé dans le script run.sh. Pas besoin d'avoir maven sur le poste pour exécuter le code.
- Utilisation, lors du job validate du pipeline de construction du jar, de deux outils de contrôle de qualité de code : [ktlint](https://github.com/pinterest/ktlint) et [detekt](https://github.com/detekt/detekt)

## Qualité du code
En application du principe "Make it work, make it right, make it fast" de Kent Beck, l'accent a d'abord été mis sur le fonctionnement de l'application, en faisant du TDD sur les principales fonctionnalités métier attendues (hors asynchronisme qui est venu par la suite). Il serait nécessaire de prendre plus de temps pour accélérer dans la phase "Right".

A ma connaissance, une code review serait nécessaire sur les points suivants :
- la durée des tâches est en dur dans des constantes dans le robot. Ce n'est sans doute pas la responsabilité du robot de détenir ces infos. Et il faudrait sans doute les mettre dans un fichier de configuration pour que ce soit plus souple.
- même chose pour le prix d'un foobar et d'un robot éparpillés dans les classes (respectivement FooBar et Foobartory)
- le package tools a un nommage trop "technique", pas suffisamment métier
- les tests qui vérifient le temps de switch entre deux activités du robot s'appuient sur des séquences d'attente, il serait plus judicieux de "donner du sens" à cette attente : avoir un objet intermédiaire entre le robot et la classe qui va vraiment attendre ?
- les deux classes de test sont grandes, on pourrait imaginer les splitter par fonctionnalité
- pas de tests sur la partie asynchrone du code (sélection d'un robot libre, ...)
- l'accès à l'état de foobartory peut être supprimé (nombre de robots, foos, bars, foobars, ...), il n'y a pas de besoin métier de les récupérer. Seuls les tests les utilisent aujourd'hui. Ca pourrait se tester de façon indirecte en utilisant la fonction qui affiche le statut de la foobartory.
- vente de foobar : si on ne peut pas vendre le nombre de foobars randomisé, car pas assez de foobars en stock, on n'en vend aucun (non visible dans les tests, à améliorer). Discuter avec le métier pour connaître le comportement attendu.

Ces différents points ont été laissés en l'état dans le code afin d'avancer dans le temps imparti.
