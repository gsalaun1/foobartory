#!/usr/bin/env bash

checkConfig() {
  if [[ -z "${JAVA_HOME}" ]]; then
    echo "La variable JAVA_HOME n'est pas déclarée"
    exit 1
  fi

  if [ ! -f $JAVA_HOME/bin/java ]; then
    echo "Le binaire java n'a pas été trouvé"
  fi
}

downloadLatest() {
  mkdir -p bin
  echo "======================================================"
  echo "Téléchargement de la dernière version de l'application"
  echo "======================================================"
  wget https://gitlab.com/gsalaun1/foobartory/-/jobs/artifacts/master/raw/foobartory-latest.jar?job=build -O bin/foobartory-latest.jar
  echo "============================================================="
  echo "La dernière version de l'application a bien été téléchargée !"
  echo "============================================================="
}

runApp() {
  clear
  $JAVA_HOME/bin/java -jar bin/foobartory-latest.jar ${FOOBARTORY_SPEED}
}

checkConfig
downloadLatest
runApp
