package gsalaun.foobartory

import gsalaun.foobartory.domain.Foobartory
import gsalaun.foobartory.domain.Robot
import gsalaun.foobartory.tools.FoobartoryRandomizer
import gsalaun.foobartory.tools.FoobartoryWaiter

/**
 * <p>ApplicationLauncher</p>
 */

fun main(vararg args: String) {

    val speed = extractSpeed(*args)

    println("Vitesse du jeu : x$speed")

    val foobartory = Foobartory(
        buildRobotsList(FoobartoryConfiguration.NB_ROBOTS_AT_INIT, speed),
        System.out,
        FoobartoryRandomizer()
    )

    @Suppress("MagicNumber")
    while (foobartory.countRobots() < FoobartoryConfiguration.NB_ROBOTS_TO_REACH) {
        when ((1..5).random()) {
            1 -> foobartory.tellARobotToMineFoo()
            2 -> foobartory.tellARobotToMineBar()
            3 -> foobartory.tellARobotToBuildAFooBar()
            4 -> foobartory.tellARobotToSellFooBars()
            5 -> foobartory.tellARobotToBuyARobot()
        }
    }

    println("Le jeu est terminé")
    foobartory.printStatus()
}

object FoobartoryConfiguration {
    const val DEFAULT_SPEED = 1f
    const val NB_ROBOTS_AT_INIT = 2
    const val NB_ROBOTS_TO_REACH = 30
}

fun extractSpeed(vararg args: String): Float {
    return if (args.isEmpty()) {
        FoobartoryConfiguration.DEFAULT_SPEED
    } else {
        try {
            args[0].toFloat()
        } catch (e: NumberFormatException) {
            FoobartoryConfiguration.DEFAULT_SPEED
        }
    }
}

fun buildRobotsList(nbRobots: Int, speed: Float): List<Robot> {
    val waiter = FoobartoryWaiter(speed)
    val randomizer = FoobartoryRandomizer()
    return (1..nbRobots).map { Robot(waiter, randomizer) }
}
