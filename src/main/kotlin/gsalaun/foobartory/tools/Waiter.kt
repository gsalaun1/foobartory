package gsalaun.foobartory.tools

/**
 * <p>Waiter</p>
 */
interface Waiter {
    fun waitFor(seconds: Float)
    fun waitBetween(minimum: Float, maximum: Float)
}
