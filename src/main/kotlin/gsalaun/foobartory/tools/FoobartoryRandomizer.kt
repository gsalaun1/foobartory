package gsalaun.foobartory.tools

/**
 * <p>FoobartoryRandomizer</p>
 */
class FoobartoryRandomizer : Randomizer {
    companion object {
        const val MAX_PERCENTAGE = 100
    }

    override fun randomizeSuccessWithSuchPercentage(percentage: Int): Boolean {
        val score = (0..MAX_PERCENTAGE).random()
        return score <= percentage
    }

    override fun determineHowMuchFooBarsToSell(maximum: Int): Int {
        return (1..maximum).random()
    }
}
