package gsalaun.foobartory.tools

import kotlin.random.Random

/**
 * <p>FoobartoryWaiter</p>
 */
class FoobartoryWaiter(private val speed: Float) : Waiter {
    companion object {
        const val MILLISECONDS_IN_A_SECOND = 1000
    }

    override fun waitFor(seconds: Float) {
        Thread.sleep((seconds * MILLISECONDS_IN_A_SECOND / speed).toLong())
    }

    override fun waitBetween(minimum: Float, maximum: Float) {
        val timeToWait = Random.nextDouble(minimum.toDouble(), maximum.toDouble())
        Thread.sleep((timeToWait * MILLISECONDS_IN_A_SECOND / speed).toLong())
    }
}
