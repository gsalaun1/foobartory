package gsalaun.foobartory.tools

/**
 * <p>Randomizer</p>
 */
interface Randomizer {
    fun randomizeSuccessWithSuchPercentage(percentage: Int): Boolean

    fun determineHowMuchFooBarsToSell(maximum: Int): Int
}
