package gsalaun.foobartory.domain

import java.util.UUID

/**
 * <p>Bar</p>
 */
class Bar(val id: UUID = UUID.randomUUID())
