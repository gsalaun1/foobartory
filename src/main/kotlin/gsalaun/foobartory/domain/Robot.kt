package gsalaun.foobartory.domain

import gsalaun.foobartory.tools.Randomizer
import gsalaun.foobartory.tools.Waiter
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

/**
 * <p>Robot</p>
 */
class Robot(private val waiter: Waiter, private val randomizer: Randomizer) {
    companion object {
        const val BAR_MINING_MAXIMUM_DURATION = 2f
        const val BAR_MINING_MINIMUM_DURATION = 0.5f
        const val FOO_MINING_DURATION = 1f
        const val FOOBAR_BUILDING_DURATION = 2f
        const val FOOBAR_BUILDING_SUCESS_PERCENTAGE = 60
        const val FOOBAR_SELLING_DURATION = 10f
        const val SWITCH_ACTION_DURATION = 5f
    }

    private var lastAction: Action? = null

    fun mineFoo(): Deferred<Foo> {
        return GlobalScope.async {
            eventuallytakeTimeToSwitchTo(Action.FOO_MINING)
            waiter.waitFor(FOO_MINING_DURATION)
            lastAction = Action.FOO_MINING
            Foo()
        }
    }

    fun mineBar(): Deferred<Bar> {
        return GlobalScope.async {
            eventuallytakeTimeToSwitchTo(Action.BAR_MINING)
            waiter.waitBetween(BAR_MINING_MINIMUM_DURATION, BAR_MINING_MAXIMUM_DURATION)
            lastAction = Action.BAR_MINING
            Bar()
        }
    }

    fun buildFooBar(foo: Foo, bar: Bar): Deferred<FooBar?> {
        return GlobalScope.async {
            eventuallytakeTimeToSwitchTo(Action.FOOBAR_BUILDING)
            waiter.waitFor(FOOBAR_BUILDING_DURATION)
            if (randomizer.randomizeSuccessWithSuchPercentage(FOOBAR_BUILDING_SUCESS_PERCENTAGE)) {
                lastAction = Action.FOOBAR_BUILDING
                FooBar(foo, bar)
            } else {
                lastAction = Action.FOOBAR_BUILDING
                null
            }
        }
    }

    fun sell(vararg fooBars: FooBar): Deferred<Int> {
        return GlobalScope.async {
            eventuallytakeTimeToSwitchTo(Action.FOOBAR_SELLING)
            waiter.waitFor(FOOBAR_SELLING_DURATION)
            lastAction = Action.FOOBAR_SELLING
            fooBars.map { it.price }.sum()
        }
    }

    fun buyRobot(price: Int, foos: List<Foo>): Deferred<Robot> {
        return GlobalScope.async {
            eventuallytakeTimeToSwitchTo(Action.ROBOT_BUYING)
            lastAction = Action.ROBOT_BUYING
            Robot(waiter, randomizer)
        }
    }

    private fun eventuallytakeTimeToSwitchTo(action: Action) {
        if (lastAction != null && lastAction != action) {
            waiter.waitFor(SWITCH_ACTION_DURATION)
        }
    }

    private enum class Action {
        BAR_MINING, FOO_MINING, FOOBAR_BUILDING, FOOBAR_SELLING, ROBOT_BUYING
    }
}
