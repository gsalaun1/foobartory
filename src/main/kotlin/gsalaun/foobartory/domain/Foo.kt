package gsalaun.foobartory.domain

import java.util.UUID

/**
 * <p>Foo</p>
 */
class Foo(val id: UUID = UUID.randomUUID())
