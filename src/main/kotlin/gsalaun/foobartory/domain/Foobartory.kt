package gsalaun.foobartory.domain

import gsalaun.foobartory.tools.Randomizer
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import java.io.PrintStream

/**
 * <p>Foobartory</p>
 */
class Foobartory(
    private val robots: List<Robot> = emptyList(),
    private val printStream: PrintStream,
    private val randomizer: Randomizer
) {

    companion object {
        const val MAXIMUM_FOOBARS_TO_SELL = 5
        const val REQUIRED_FOOS_TO_BUY_A_ROBOT = 6
        const val REQUIRED_MONEY_TO_BUY_A_ROBOT = 3
    }

    private val foos = mutableListOf<Foo>()
    private val bars = mutableListOf<Bar>()
    private val fooBars = mutableListOf<FooBar>()
    private var money = 0

    private val availableRobots = mutableListOf<Robot>()
    private val unAvailableRobots = mutableListOf<Robot>()

    private val mutex = Mutex()

    init {
        availableRobots.addAll(robots)
        printStream.println("Création de la foobartory")
    }

    fun countRobots() = availableRobots.size + unAvailableRobots.size

    fun fooStocks() = foos.size

    fun barStocks() = bars.size

    fun fooBarStocks() = fooBars.size

    fun availableMoney() = money

    fun tellARobotToMineFoo(): Deferred<Unit> {
        return GlobalScope.async {
            val robot = pickARobot()
            if (robot != null) {
                val foo = robot.mineFoo().await()
                foos.add(foo)
                printStream.println("Un robot a miné un Foo")
                printStatus()
                releaseRobot(robot)
            }
        }
    }

    fun tellARobotToMineBar(): Deferred<Unit> {
        return GlobalScope.async {
            val robot = pickARobot()
            if (robot != null) {
                val bar = robot.mineBar().await()
                bars.add(bar)
                printStream.println("Un robot a miné un Bar")
                printStatus()
                releaseRobot(robot)
            }
        }
    }

    fun tellARobotToBuildAFooBar(): Deferred<Unit> {
        return GlobalScope.async {
            val (foo, bar) = pickAFooAndABar()
            if (foo != null && bar != null) {
                val robot = pickARobot()
                if (robot != null) {
                    val fooBar = robot.buildFooBar(foo, bar).await()
                    if (fooBar != null) {
                        fooBars.add(fooBar)
                        printStream.println("Un robot a réussi à construire un foobar")
                        printStatus()
                    } else {
                        bars.add(bar)
                        printStream.println("Un robot a échoué à construire un foobar")
                        printStatus()
                    }
                    releaseRobot(robot)
                }
            }
        }
    }

    fun tellARobotToSellFooBars(): Deferred<Unit> {
        return GlobalScope.async {
            val nbFooBarsToSell = randomizer.determineHowMuchFooBarsToSell(MAXIMUM_FOOBARS_TO_SELL)
            val pickedFooBars = pickFooBars(nbFooBarsToSell)
            val robot = pickARobot()
            if (pickedFooBars.isNotEmpty() && robot != null) {
                val wonMoney = robot.sell(*(pickedFooBars.toTypedArray())).await()
                mutex.withLock {
                    money += wonMoney
                }
                printStream.println("Un robot a vendu $nbFooBarsToSell foobar(s)")
                printStatus()
            }
            if (robot != null) {
                releaseRobot(robot)
            }
        }
    }

    fun tellARobotToBuyARobot(): Deferred<Unit> {
        return GlobalScope.async {
            val enoughMoney = operateOnMoney(-REQUIRED_MONEY_TO_BUY_A_ROBOT)
            val pickedFoos = pickFoosAndBars(REQUIRED_FOOS_TO_BUY_A_ROBOT, 0)
            val robot = pickARobot()
            if (enoughMoney && pickedFoos.first.size == REQUIRED_FOOS_TO_BUY_A_ROBOT && robot != null) {
                val newRobot = robot.buyRobot(REQUIRED_MONEY_TO_BUY_A_ROBOT, pickedFoos.first).await()
                availableRobots.add(newRobot)
                releaseRobot(robot)
                printStream.println("Un robot a été acheté")
                printStatus()
            } else {
                if (robot != null) {
                    releaseRobot(robot)
                }
                operateOnMoney(REQUIRED_MONEY_TO_BUY_A_ROBOT)
                pickedFoos.first.forEach { foos.add(it) }
            }
        }
    }

    fun printStatus() {
        var statusRobots = "${countRobots()} robot"
        if (countRobots() > 1) {
            statusRobots += "s"
        }
        var statusMoney = "$money euro"
        if (money > 1) {
            statusMoney += "s"
        }
        printStream.println(
            "Etat : $statusRobots - ${foos.size} Foo - ${bars.size} Bar - ${fooBars.size} FooBar - $statusMoney"
        )
    }

    private suspend fun pickARobot(): Robot? {
        return mutex.withLock {
            if (availableRobots.isEmpty()) {
                null
            } else {
                val robot = availableRobots.removeAt(0)
                unAvailableRobots.add(robot)
                robot
            }
        }
    }

    private fun releaseRobot(robot: Robot) {
        unAvailableRobots.remove(robot)
        availableRobots.add(robot)
    }

    private suspend fun pickAFooAndABar(): Pair<Foo?, Bar?> {
        val picked = pickFoosAndBars(1, 1)
        val foo = if (picked.first.isNotEmpty()) {
            picked.first[0]
        } else {
            null
        }
        val bar = if (picked.second.isNotEmpty()) {
            picked.second[0]
        } else {
            null
        }
        return Pair(foo, bar)
    }

    private suspend fun pickFoosAndBars(nbFoos: Int, nbBars: Int): Pair<List<Foo>, List<Bar>> {
        return mutex.withLock {
            val pickedFoos: List<Foo> =
                if (nbFoos > foos.size) {
                    emptyList()
                } else {
                    val currentFoos = mutableListOf<Foo>()
                    for (i in (1..nbFoos)) {
                        currentFoos.add(foos.removeAt(0))
                    }
                    currentFoos
                }
            val pickedBars: List<Bar> =
                if (nbBars > bars.size) {
                    emptyList()
                } else {
                    val currentBars = mutableListOf<Bar>()
                    for (i in (1..nbBars)) {
                        currentBars.add(bars.removeAt(0))
                    }
                    currentBars
                }
            Pair(pickedFoos, pickedBars)
        }
    }

    private suspend fun pickFooBars(nbFooBars: Int): List<FooBar> {
        return mutex.withLock {
            if (fooBars.size >= nbFooBars) {
                val result = mutableListOf<FooBar>()
                for (i in (1..nbFooBars)) {
                    result.add(fooBars.removeAt(0))
                }
                result
            } else {
                emptyList()
            }
        }
    }

    private suspend fun operateOnMoney(value: Int): Boolean {
        return mutex.withLock {
            if (value > 0) {
                money += value
                true
            } else {
                if (money + value >= 0) {
                    money += value
                    true
                } else {
                    false
                }
            }
        }
    }
}
