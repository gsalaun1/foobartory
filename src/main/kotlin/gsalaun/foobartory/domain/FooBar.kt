package gsalaun.foobartory.domain

/**
 * <p>Foobar</p>
 */
class FooBar(val foo: Foo, val bar: Bar, val price: Int = 1)
