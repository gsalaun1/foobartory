package gsalaun.foobartory.tools

/**
 * <p>WaiterMock</p>
 */
class WaiterMock : Waiter {

    private val actions = mutableListOf<WaitingAction>()

    fun hasWaitedFor(duration: Float): Boolean {
        return actions.size == 1 && actions.contains(ForWaitingAction(duration))
    }

    fun hasWaitedBetween(minimum: Float, maximum: Float): Boolean {
        return actions.size == 1 && actions.contains(BetweenWaitingAction(minimum, maximum))
    }

    fun hasSuccessivelyWaited(vararg awaitedActions: WaitingAction): Boolean {
        return actions == awaitedActions.toList()
    }

    fun hasNotWaitedForASpecificDuration(): Boolean {
        return !actions.map { it::class }.contains(ForWaitingAction::class)
    }

    override fun waitFor(seconds: Float) {
        actions.add(ForWaitingAction(seconds))
    }

    override fun waitBetween(minimum: Float, maximum: Float) {
        actions.add(BetweenWaitingAction(minimum, maximum))
    }
}

sealed class WaitingAction
data class ForWaitingAction(val duration: Float) : WaitingAction()
data class BetweenWaitingAction(val minimum: Float, val maximum: Float) : WaitingAction()
