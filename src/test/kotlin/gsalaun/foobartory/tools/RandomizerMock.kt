package gsalaun.foobartory.tools

/**
 * <p>RandomizerMock</p>
 */
class RandomizerMock(val success: Boolean = true, val fooBarsToSell: Int = 1) : Randomizer {

    var lastCalledPercentage: Int? = null

    override fun randomizeSuccessWithSuchPercentage(percentage: Int): Boolean {
        lastCalledPercentage = percentage
        return success
    }

    override fun determineHowMuchFooBarsToSell(maximum: Int) = fooBarsToSell
}
