package gsalaun.foobartory

import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.assertions.isNotNull
import assertk.assertions.isTrue
import gsalaun.foobartory.domain.Foobartory
import gsalaun.foobartory.domain.Robot
import gsalaun.foobartory.tools.BetweenWaitingAction
import gsalaun.foobartory.tools.ForWaitingAction
import gsalaun.foobartory.tools.RandomizerMock
import gsalaun.foobartory.tools.WaiterMock
import gsalaun.foobartory.tools.Randomizer
import gsalaun.foobartory.tools.Waiter
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import java.io.PrintStream

class FoobartoryTest {

    private fun buildRobotsList(nbRobots: Int, waiter: Waiter, randomizer: Randomizer): List<Robot> {
        return (1..nbRobots).map { Robot(waiter, randomizer) }
    }

    @Test
    fun `Should create Foobartory`() {
        val foobartory =
            Foobartory(buildRobotsList(1, WaiterMock(), RandomizerMock()), PrintStreamMock(), RandomizerMock())
        assertThat(foobartory).isNotNull()
    }

    @Test
    fun `Should contains one robot at init`() {
        val foobartory =
            Foobartory(buildRobotsList(1, WaiterMock(), RandomizerMock()), PrintStreamMock(), RandomizerMock())
        assertThat(foobartory.countRobots()).isEqualTo(1)
    }

    @Test
    fun `Should have no stock of foo at init`() {
        val foobartory =
            Foobartory(buildRobotsList(1, WaiterMock(), RandomizerMock()), PrintStreamMock(), RandomizerMock())
        assertThat(foobartory.fooStocks()).isEqualTo(0)
    }

    @Test
    fun `Should have no stock of bar at init`() {
        val foobartory =
            Foobartory(buildRobotsList(1, WaiterMock(), RandomizerMock()), PrintStreamMock(), RandomizerMock())
        assertThat(foobartory.barStocks()).isEqualTo(0)
    }

    @Test
    fun `Should have a foo if a robot built one`() {
        runBlocking {
            val foobartory =
                Foobartory(buildRobotsList(1, WaiterMock(), RandomizerMock()), PrintStreamMock(), RandomizerMock())
            foobartory.tellARobotToMineFoo().await()
            assertThat(foobartory.fooStocks()).isEqualTo(1)
        }
    }

    @Test
    fun `Should have waited the required time for having foo built by a robot`() {
        runBlocking {
            val waiter = WaiterMock()
            val foobartory =
                Foobartory(buildRobotsList(1, waiter, RandomizerMock()), PrintStreamMock(), RandomizerMock())
            foobartory.tellARobotToMineFoo().await()
            assertThat(waiter.hasWaitedFor(1f)).isTrue()
        }
    }

    @Test
    fun `Should have two foo if two robots built one`() {
        runBlocking {
            val foobartory =
                Foobartory(buildRobotsList(2, WaiterMock(), RandomizerMock()), PrintStreamMock(), RandomizerMock())
            foobartory.tellARobotToMineFoo().await()
            foobartory.tellARobotToMineFoo().await()
            assertThat(foobartory.fooStocks()).isEqualTo(2)
        }
    }

    @Test
    fun `Should have waited the required time for having two foos built by two robots`() {
        runBlocking {
            val waiter = WaiterMock()
            val foobartory =
                Foobartory(buildRobotsList(2, waiter, RandomizerMock()), PrintStreamMock(), RandomizerMock())
            foobartory.tellARobotToMineFoo().await()
            foobartory.tellARobotToMineFoo().await()
            assertThat(waiter.hasSuccessivelyWaited(ForWaitingAction(1f), ForWaitingAction(1f))).isTrue()
        }
    }

    @Test
    fun `Should have one bar if a robot built one`() {
        runBlocking {
            val foobartory =
                Foobartory(buildRobotsList(1, WaiterMock(), RandomizerMock()), PrintStreamMock(), RandomizerMock())
            foobartory.tellARobotToMineBar().await()
            assertThat(foobartory.barStocks()).isEqualTo(1)
        }
    }

    @Test
    fun `Should have waited the required time for having bar built by a robot`() {
        runBlocking {
            val waiter = WaiterMock()
            val foobartory =
                Foobartory(buildRobotsList(1, waiter, RandomizerMock()), PrintStreamMock(), RandomizerMock())
            foobartory.tellARobotToMineBar().await()
            assertThat(waiter.hasWaitedBetween(0.5f, 2f)).isTrue()
        }
    }

    @Test
    fun `Should have two bars if two robots built one`() {
        runBlocking {
            val foobartory =
                Foobartory(buildRobotsList(2, WaiterMock(), RandomizerMock()), PrintStreamMock(), RandomizerMock())
            foobartory.tellARobotToMineBar().await()
            foobartory.tellARobotToMineBar().await()
            assertThat(foobartory.barStocks()).isEqualTo(2)
        }
    }

    @Test
    fun `Should have waited the required time for having two bars built by two robots`() {
        runBlocking {
            val waiter = WaiterMock()
            val foobartory =
                Foobartory(buildRobotsList(2, waiter, RandomizerMock()), PrintStreamMock(), RandomizerMock())
            foobartory.tellARobotToMineBar().await()
            foobartory.tellARobotToMineBar().await()
            assertThat(
                waiter.hasSuccessivelyWaited(
                    BetweenWaitingAction(0.5f, 2f),
                    BetweenWaitingAction(0.5f, 2f)
                )
            ).isTrue()
        }
    }

    @Test
    fun `Should not contain a foobar in stock when a robot has to build a foobar and no foo available`() {
        runBlocking {
            val foobartory =
                Foobartory(buildRobotsList(1, WaiterMock(), RandomizerMock()), PrintStreamMock(), RandomizerMock())
            foobartory.tellARobotToBuildAFooBar().await()
            assertThat(foobartory.fooBarStocks()).isEqualTo(0)
        }
    }

    @Test
    fun `Should not contain a foobar in stock when a robot has to build a foobar and no bar available`() {
        runBlocking {
            val foobartory =
                Foobartory(buildRobotsList(1, WaiterMock(), RandomizerMock()), PrintStreamMock(), RandomizerMock())
            foobartory.tellARobotToMineFoo().await()
            foobartory.tellARobotToBuildAFooBar().await()
            assertThat(foobartory.fooBarStocks()).isEqualTo(0)
        }
    }

    @Test
    fun `Should contain a foobar in stock when a robot has to build a foobar and foo and bar available`() {
        runBlocking {
            val foobartory =
                Foobartory(buildRobotsList(1, WaiterMock(), RandomizerMock()), PrintStreamMock(), RandomizerMock())
            foobartory.tellARobotToMineFoo().await()
            foobartory.tellARobotToMineBar().await()
            foobartory.tellARobotToBuildAFooBar().await()
            assertThat(foobartory.fooStocks()).isEqualTo(0)
            assertThat(foobartory.barStocks()).isEqualTo(0)
            assertThat(foobartory.fooBarStocks()).isEqualTo(1)
        }
    }

    @Test
    fun `Should not contain a foobar in stock when a robot has failed to build a foobar and foo and bar available`() {
        runBlocking {
            val foobartory =
                Foobartory(buildRobotsList(1, WaiterMock(), RandomizerMock(false)), PrintStreamMock(), RandomizerMock())
            foobartory.tellARobotToMineFoo().await()
            foobartory.tellARobotToMineBar().await()
            foobartory.tellARobotToBuildAFooBar().await()
            assertThat(foobartory.fooStocks()).isEqualTo(0)
            assertThat(foobartory.barStocks()).isEqualTo(1)
            assertThat(foobartory.fooBarStocks()).isEqualTo(0)
        }
    }

    @Test
    fun `Should sell one foobar`() {
        runBlocking {
            val foobartory =
                Foobartory(buildRobotsList(1, WaiterMock(), RandomizerMock()), PrintStreamMock(), RandomizerMock())
            foobartory.tellARobotToMineFoo().await()
            foobartory.tellARobotToMineBar().await()
            foobartory.tellARobotToBuildAFooBar().await()
            foobartory.tellARobotToSellFooBars().await()
            assertThat(foobartory.fooStocks()).isEqualTo(0)
            assertThat(foobartory.barStocks()).isEqualTo(0)
            assertThat(foobartory.fooBarStocks()).isEqualTo(0)
            assertThat(foobartory.availableMoney()).isEqualTo(1)
        }
    }

    @Test
    fun `Should sell five foobars`() {
        runBlocking {
            val foobartory =
                Foobartory(
                    buildRobotsList(1, WaiterMock(), RandomizerMock()),
                    PrintStreamMock(),
                    RandomizerMock(fooBarsToSell = 5)
                )
            (1..5).forEach { _ -> foobartory.tellARobotToMineFoo().await() }
            (1..5).forEach { _ -> foobartory.tellARobotToMineBar().await() }
            (1..5).forEach { _ -> foobartory.tellARobotToBuildAFooBar().await() }
            foobartory.tellARobotToSellFooBars().await()
            assertThat(foobartory.fooStocks()).isEqualTo(0)
            assertThat(foobartory.barStocks()).isEqualTo(0)
            assertThat(foobartory.fooBarStocks()).isEqualTo(0)
            assertThat(foobartory.availableMoney()).isEqualTo(5)
        }
    }

    @Test
    fun `Should buy a robot`() {
        runBlocking {
            val foobartory =
                Foobartory(
                    buildRobotsList(1, WaiterMock(), RandomizerMock()),
                    PrintStreamMock(),
                    RandomizerMock(fooBarsToSell = 3)
                )
            (1..9).forEach { _ -> foobartory.tellARobotToMineFoo().await() }
            (1..3).forEach { _ -> foobartory.tellARobotToMineBar().await() }
            (1..3).forEach { _ -> foobartory.tellARobotToBuildAFooBar().await() }
            (1..3).forEach { _ -> foobartory.tellARobotToSellFooBars().await() }
            foobartory.tellARobotToBuyARobot().await()
            assertThat(foobartory.countRobots()).isEqualTo(2)
            assertThat(foobartory.fooStocks()).isEqualTo(0)
            assertThat(foobartory.barStocks()).isEqualTo(0)
            assertThat(foobartory.fooBarStocks()).isEqualTo(0)
            assertThat(foobartory.availableMoney()).isEqualTo(0)
        }
    }

    @Test
    fun `Should print correct status at init`() {
        val printStream = PrintStreamMock()
        val foobartory = Foobartory(buildRobotsList(1, WaiterMock(), RandomizerMock()), printStream, RandomizerMock())
        foobartory.printStatus()
        assertThat(printStream.lastPrinted()).isEqualTo("Etat : 1 robot - 0 Foo - 0 Bar - 0 FooBar - 0 euro")
    }

    @Test
    fun `Should print correct status at init if two robots`() {
        val printStream = PrintStreamMock()
        val foobartory = Foobartory(buildRobotsList(2, WaiterMock(), RandomizerMock()), printStream, RandomizerMock())
        foobartory.printStatus()
        assertThat(printStream.lastPrinted()).isEqualTo("Etat : 2 robots - 0 Foo - 0 Bar - 0 FooBar - 0 euro")
    }

    @Test
    fun `Should print correct status after mining a foo`() {
        runBlocking {
            val printStream = PrintStreamMock()
            val foobartory =
                Foobartory(buildRobotsList(1, WaiterMock(), RandomizerMock()), printStream, RandomizerMock())
            foobartory.tellARobotToMineFoo().await()
            assertThat(printStream.lastPrinted()).isEqualTo("Etat : 1 robot - 1 Foo - 0 Bar - 0 FooBar - 0 euro")
        }
    }

    @Test
    fun `Should print correct status after mining a bar`() {
        runBlocking {
            val printStream = PrintStreamMock()
            val foobartory =
                Foobartory(buildRobotsList(1, WaiterMock(), RandomizerMock()), printStream, RandomizerMock())
            foobartory.tellARobotToMineBar().await()
            assertThat(printStream.lastPrinted()).isEqualTo("Etat : 1 robot - 0 Foo - 1 Bar - 0 FooBar - 0 euro")
        }
    }

    @Test
    fun `Should print correct status after building a foobar`() {
        runBlocking {
            val printStream = PrintStreamMock()
            val foobartory =
                Foobartory(buildRobotsList(1, WaiterMock(), RandomizerMock(true)), printStream, RandomizerMock())
            foobartory.tellARobotToMineFoo().await()
            foobartory.tellARobotToMineBar().await()
            foobartory.tellARobotToBuildAFooBar().await()
            assertThat(printStream.lastPrinted()).isEqualTo("Etat : 1 robot - 0 Foo - 0 Bar - 1 FooBar - 0 euro")
        }
    }

    @Test
    fun `Should print correct status after selling a foobar`() {
        runBlocking {
            val printStream = PrintStreamMock()
            val foobartory =
                Foobartory(buildRobotsList(1, WaiterMock(), RandomizerMock()), printStream, RandomizerMock())
            foobartory.tellARobotToMineFoo().await()
            foobartory.tellARobotToMineBar().await()
            foobartory.tellARobotToBuildAFooBar().await()
            foobartory.tellARobotToSellFooBars().await()
            assertThat(printStream.lastPrinted()).isEqualTo("Etat : 1 robot - 0 Foo - 0 Bar - 0 FooBar - 1 euro")
        }
    }

    @Test
    fun `Should print correct status after selling five foobars`() {
        runBlocking {
            val printStream = PrintStreamMock()
            val foobartory =
                Foobartory(
                    buildRobotsList(1, WaiterMock(), RandomizerMock()),
                    printStream,
                    RandomizerMock(fooBarsToSell = 5)
                )
            (1..5).forEach { _ -> foobartory.tellARobotToMineFoo().await() }
            (1..5).forEach { _ -> foobartory.tellARobotToMineBar().await() }
            (1..5).forEach { _ -> foobartory.tellARobotToBuildAFooBar().await() }
            foobartory.tellARobotToSellFooBars().await()
            assertThat(printStream.lastPrinted()).isEqualTo("Etat : 1 robot - 0 Foo - 0 Bar - 0 FooBar - 5 euros")
        }
    }

    @Test
    fun `Should print correct status after buying a robot`() {
        runBlocking {
            val printStream = PrintStreamMock()
            val foobartory =
                Foobartory(
                    buildRobotsList(1, WaiterMock(), RandomizerMock()),
                    printStream,
                    RandomizerMock(fooBarsToSell = 3)
                )
            (1..9).forEach { _ -> foobartory.tellARobotToMineFoo().await() }
            (1..3).forEach { _ -> foobartory.tellARobotToMineBar().await() }
            (1..3).forEach { _ -> foobartory.tellARobotToBuildAFooBar().await() }
            (1..3).forEach { _ -> foobartory.tellARobotToSellFooBars().await() }
            foobartory.tellARobotToBuyARobot().await()
            assertThat(printStream.lastPrinted()).isEqualTo("Etat : 2 robots - 0 Foo - 0 Bar - 0 FooBar - 0 euro")
        }
    }
}

private class PrintStreamMock : PrintStream(System.out) {

    private var lastPrintedMessage: String? = null

    override fun println(message: String?) {
        lastPrintedMessage = message
    }

    fun lastPrinted() = lastPrintedMessage
}
