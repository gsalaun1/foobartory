package gsalaun.foobartory

import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.assertions.isNotNull
import assertk.assertions.isNull
import assertk.assertions.isTrue
import gsalaun.foobartory.domain.Bar
import gsalaun.foobartory.domain.Foo
import gsalaun.foobartory.domain.FooBar
import gsalaun.foobartory.domain.Robot
import gsalaun.foobartory.tools.BetweenWaitingAction
import gsalaun.foobartory.tools.ForWaitingAction
import gsalaun.foobartory.tools.RandomizerMock
import gsalaun.foobartory.tools.WaiterMock
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test

/**
 * <p>RobotTest</p>
 */
class RobotTest {

    @Test
    fun `Should create robot`() {
        val robot = Robot(WaiterMock(), RandomizerMock())
        assertThat(robot).isNotNull()
    }

    @Test
    fun `Should mine foo`() {
        runBlocking {
            val robot = Robot(WaiterMock(), RandomizerMock())
            val foo = robot.mineFoo().await()
            assertThat(foo).isNotNull()
        }
    }

    @Test
    fun `Should mine foo during the expected time`() {
        runBlocking {
            val waiter = WaiterMock()
            val robot = Robot(waiter, RandomizerMock())
            robot.mineFoo().await()
            assertThat(waiter.hasWaitedFor(1f)).isTrue()
        }
    }

    @Test
    fun `Should mine bar`() {
        runBlocking {
            val robot = Robot(WaiterMock(), RandomizerMock())
            val bar = robot.mineBar().await()
            assertThat(bar).isNotNull()
        }
    }

    @Test
    fun `Should mine bar during the expected time`() {
        runBlocking {
            val waiter = WaiterMock()
            val robot = Robot(waiter, RandomizerMock())
            robot.mineBar().await()
            assertThat(waiter.hasWaitedBetween(0.5f, 2f)).isTrue()
        }
    }

    @Test
    fun `Should take time while switching from foo mining to bar mining`() {
        runBlocking {
            val waiter = WaiterMock()
            val robot = Robot(waiter, RandomizerMock())
            robot.mineFoo().await()
            robot.mineBar().await()
            assertThat(
                waiter.hasSuccessivelyWaited(
                    ForWaitingAction(1f),
                    ForWaitingAction(5f),
                    BetweenWaitingAction(0.5f, 2f)
                )
            ).isTrue()
        }
    }

    @Test
    fun `Should take time while switching from bar mining to foo mining`() {
        runBlocking {
            val waiter = WaiterMock()
            val robot = Robot(waiter, RandomizerMock())
            robot.mineBar().await()
            robot.mineFoo().await()
            assertThat(
                waiter.hasSuccessivelyWaited(
                    BetweenWaitingAction(0.5f, 2f),
                    ForWaitingAction(5f),
                    ForWaitingAction(1f)
                )
            ).isTrue()
        }
    }

    @Test
    fun `Should not take time to mine foo if it's the single task`() {
        runBlocking {
            val waiter = WaiterMock()
            val robot = Robot(waiter, RandomizerMock())
            robot.mineFoo().await()
            assertThat(waiter.hasWaitedFor(1f)).isTrue()
        }
    }

    @Test
    fun `Should not take time to mine bar if it's the single task`() {
        runBlocking {
            val waiter = WaiterMock()
            val robot = Robot(waiter, RandomizerMock())
            robot.mineBar().await()
            assertThat(waiter.hasNotWaitedForASpecificDuration()).isTrue()
        }
    }

    @Test
    fun `Should not take time between two foo minings`() {
        runBlocking {
            val waiter = WaiterMock()
            val robot = Robot(waiter, RandomizerMock())
            robot.mineFoo().await()
            robot.mineFoo().await()
            assertThat(
                waiter.hasSuccessivelyWaited(
                    ForWaitingAction(1f),
                    ForWaitingAction(1f)
                )
            ).isTrue()
        }
    }

    @Test
    fun `Should not take time between two bar minings`() {
        runBlocking {
            val waiter = WaiterMock()
            val robot = Robot(waiter, RandomizerMock())
            robot.mineBar().await()
            robot.mineBar().await()
            assertThat(waiter.hasNotWaitedForASpecificDuration()).isTrue()
        }
    }

    @Test
    fun `Should produce a foobar at 60 percent rate`() {
        runBlocking {
            val randomizer = RandomizerMock()
            val robot = Robot(WaiterMock(), randomizer)
            robot.buildFooBar(Foo(), Bar()).await()
            assertThat(randomizer.lastCalledPercentage).isEqualTo(60)
        }
    }

    @Test
    fun `Should produce a foobar`() {
        runBlocking {
            val randomizer = RandomizerMock(true)
            val robot = Robot(WaiterMock(), randomizer)
            val fooBar = robot.buildFooBar(Foo(), Bar()).await()
            assertThat(fooBar).isNotNull()
        }
    }

    @Test
    fun `Should not produce a foobar`() {
        runBlocking {
            val randomizer = RandomizerMock(false)
            val robot = Robot(WaiterMock(), randomizer)
            val fooBar = robot.buildFooBar(Foo(), Bar()).await()
            assertThat(fooBar).isNull()
        }
    }

    @Test
    fun `Should take time to produce a foobar`() {
        runBlocking {
            val waiter = WaiterMock()
            val robot = Robot(waiter, RandomizerMock())
            robot.buildFooBar(Foo(), Bar()).await()
            assertThat(waiter.hasWaitedFor(2f)).isTrue()
        }
    }

    @Test
    fun `Should take time while switching from foo mining to foobar building`() {
        runBlocking {
            val waiter = WaiterMock()
            val robot = Robot(waiter, RandomizerMock())
            robot.mineFoo().await()
            robot.buildFooBar(Foo(), Bar()).await()
            assertThat(
                waiter.hasSuccessivelyWaited(
                    ForWaitingAction(1f),
                    ForWaitingAction(5f),
                    ForWaitingAction(2f)
                )
            ).isTrue()
        }
    }

    @Test
    fun `Should take time while switching from bar mining to foobar building`() {
        runBlocking {
            val waiter = WaiterMock()
            val robot = Robot(waiter, RandomizerMock())
            robot.mineBar().await()
            robot.buildFooBar(Foo(), Bar()).await()
            assertThat(
                waiter.hasSuccessivelyWaited(
                    BetweenWaitingAction(0.5f, 2f),
                    ForWaitingAction(5f),
                    ForWaitingAction(2f)
                )
            ).isTrue()
        }
    }

    @Test
    fun `Should take not time between two foobar buildings`() {
        runBlocking {
            val waiter = WaiterMock()
            val robot = Robot(waiter, RandomizerMock())
            robot.buildFooBar(Foo(), Bar()).await()
            robot.buildFooBar(Foo(), Bar()).await()
            assertThat(
                waiter.hasSuccessivelyWaited(
                    ForWaitingAction(2f),
                    ForWaitingAction(2f)
                )
            ).isTrue()
        }
    }

    @Test
    fun `Should sell one fooBar`() {
        runBlocking {
            val robot = Robot(WaiterMock(), RandomizerMock())
            val money = robot.sell(FooBar(Foo(), Bar())).await()
            assertThat(money).isEqualTo(1)
        }
    }

    @Test
    fun `Should sell five fooBars`() {
        runBlocking {
            val robot = Robot(WaiterMock(), RandomizerMock())
            val money = robot.sell(
                FooBar(Foo(), Bar()),
                FooBar(Foo(), Bar()),
                FooBar(Foo(), Bar()),
                FooBar(Foo(), Bar()),
                FooBar(Foo(), Bar())
            ).await()
            assertThat(money).isEqualTo(5)
        }
    }

    @Test
    fun `Should wait the expected time to sell fooBar`() {
        runBlocking {
            val waiter = WaiterMock()
            val robot = Robot(waiter, RandomizerMock())
            robot.sell(FooBar(Foo(), Bar())).await()
            assertThat(waiter.hasWaitedFor(10f)).isTrue()
        }
    }

    @Test
    fun `Should take time while switching from foo mining to foobar selling`() {
        runBlocking {
            val waiter = WaiterMock()
            val robot = Robot(waiter, RandomizerMock())
            robot.mineFoo().await()
            robot.sell(FooBar(Foo(), Bar())).await()
            assertThat(
                waiter.hasSuccessivelyWaited(
                    ForWaitingAction(1f),
                    ForWaitingAction(5f),
                    ForWaitingAction(10f)
                )
            ).isTrue()
        }
    }

    @Test
    fun `Should not take time between two foobar sellings`() {
        runBlocking {
            val waiter = WaiterMock()
            val robot = Robot(waiter, RandomizerMock())
            robot.sell(FooBar(Foo(), Bar())).await()
            robot.sell(FooBar(Foo(), Bar())).await()
            assertThat(
                waiter.hasSuccessivelyWaited(
                    ForWaitingAction(10f),
                    ForWaitingAction(10f)
                )
            ).isTrue()
        }
    }

    @Test
    fun `Should buy robot`() {
        runBlocking {
            val waiter = WaiterMock()
            val robot = Robot(waiter, RandomizerMock())
            var buyedRobot = robot.buyRobot(3, listOf(Foo(), Foo(), Foo(), Foo(), Foo(), Foo())).await()
            assertThat(buyedRobot).isNotNull()
        }
    }

    @Test
    fun `Should not wait to buy a robot`() {
        runBlocking {
            val waiter = WaiterMock()
            val robot = Robot(waiter, RandomizerMock())
            robot.buyRobot(3, listOf(Foo(), Foo(), Foo(), Foo(), Foo(), Foo())).await()
            assertThat(waiter.hasNotWaitedForASpecificDuration()).isTrue()
        }
    }

    @Test
    fun `Should take time while switching from foo mining to robot buying`() {
        runBlocking {
            val waiter = WaiterMock()
            val robot = Robot(waiter, RandomizerMock())
            robot.mineFoo().await()
            robot.buyRobot(3, listOf(Foo(), Foo(), Foo(), Foo(), Foo(), Foo())).await()
            assertThat(
                waiter.hasSuccessivelyWaited(ForWaitingAction(1f), ForWaitingAction(5f))
            ).isTrue()
        }
    }

    @Test
    fun `Should not take time between two robot buyings`() {
        runBlocking {
            val waiter = WaiterMock()
            val robot = Robot(waiter, RandomizerMock())
            robot.buyRobot(3, listOf(Foo(), Foo(), Foo(), Foo(), Foo(), Foo())).await()
            robot.buyRobot(3, listOf(Foo(), Foo(), Foo(), Foo(), Foo(), Foo())).await()
            assertThat(
                waiter.hasNotWaitedForASpecificDuration()
            ).isTrue()
        }
    }
}
